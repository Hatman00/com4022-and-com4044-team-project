This is our group project.<br>
<br>
Here are the directories:<br>
    /Presentation: This contains the plan of the project as a presentation. (Which was the finale of our business module)<br>
    /Project: This contains the project itself.<br>
    /Report: This contains the report of the project plan. (I.E: The project breifing and such) The report of the project itself is in /Project/Website Report.docx<br>
<br>
Here are the subdirectories:<br>
    /Project/db: This contains the database file (called scooters.sql) which will be used by Docker Compose.<br>
    /Project/Design: This contains design files for some parts of the website. Either to be used as a reference while programming the website or in the report.<br>
    /Project/ScooterStack: Where the main files for the website are stored. Docker Compose reads this to start the website.<br>
<br>
And finally, the subdirectories of /Project/ScooterStack:<br>
    /Project/ScooterStack/__pycache__: Contains cache used by Python.<br>
    /Project/ScooterStack/Include: Unknown purpose. Contains nothing.<br>
    /Project/ScooterStack/Lib: Contains the libraries used by the website. (Only used when in using virtual mode (I.E: /Project/ScooterStack/Scripts.ps1))<br>
    /Project/ScooterStack/Scripts: Contains the executables for the libraries along with the virtual mode.<br>
    /Project/ScooterStack/static: Contains the static files called by the website. These include images, JavaScript and CSS.<br>
    /Project/ScooterStack/templates: Contains the HTML files used by the website. "layout.html" is the main HTML that is always displayed.<br>