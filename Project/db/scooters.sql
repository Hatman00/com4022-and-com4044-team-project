/* Create the database */
drop database if exists scooterstack;
create database scooterstack;
use scooterstack;

/* Create scooter table */
drop table if exists scooters;
CREATE TABLE scooters(
    scooter_id int NOT NULL,
    available boolean NOT NULL,
    charge int NOT NULL,
    times_used int,
    PRIMARY KEY(scooter_id)
);

/* Create depots table */
drop table if exists depots;
CREATE TABLE depots(
    depot_id int NOT NULL,
    depot_name varchar(120) NOT NULL,
    depot_location varchar(400) NOT NULL,
    depot_open boolean NOT NULL,
    depot_imagename varchar(128) NOT NULL,
    PRIMARY KEY(depot_id)
);

/* Create users table */
drop table if exists users;
CREATE TABLE users(
    user_id_no int NOT NULL,
    user_forename varchar(255) NOT NULL,
    user_surname varchar(255) NOT NULL,
    user_uname varchar(100) NOT NULL,
    user_pass varchar(100) NOT NULL,
    user_email varchar(128),
    user_phone_no varchar(12),
    PRIMARY KEY(user_id_no)
);

/* LINK TABLES */
/* Create scooter_depot link table */
drop table if exists scooter_depot;
CREATE TABLE scooter_depot(
    scooter_id int NOT NULL,
    depot_id int NOT NULL,
    PRIMARY KEY(scooter_id, depot_id),
    FOREIGN KEY(scooter_id) REFERENCES scooters(scooter_id),
    FOREIGN KEY(depot_id) REFERENCES depots(depot_id)
);

/* Create user_depots link table */
drop table if exists user_depots;
CREATE TABLE user_depots(
    user_id_no int NOT NULL,
    depot_id int NOT NULL,
    PRIMARY KEY(user_id_no, depot_id),
    FOREIGN KEY(user_id_no) REFERENCES users(user_id_no),
    FOREIGN KEY(depot_id) REFERENCES depots(depot_id)
);

/* Create user_scooters link table */
drop table if exists user_scooters;
CREATE TABLE user_scooters(
    scooter_id int NOT NULL,
    user_id_no int NOT NULL,
    PRIMARY KEY(scooter_id, user_id_no),
    FOREIGN KEY(scooter_id) REFERENCES scooters(scooter_id),
    FOREIGN KEY(user_id_no) REFERENCES users(user_id_no)
);

/* DEFAULT VALUES */
/* scooters table values */
INSERT INTO scooters VALUES(
    0, true, 100, 0),
    (1, true, 100, 0),
    (2, true, 100, 0),
    (3, true, 100, 0),
    (4, true, 100, 0),
    (5, true, 100, 0),
    (6, true, 100, 0),
    (7, true, 100, 0),
    (8, true, 100, 0),
    (9, true, 100, 0),
    (10, true, 100, 0),
    (11, true, 100, 0),
    (12, true, 100, 0),
    (13, true, 100, 0),
    (14, true, 100, 0),
    (15, true, 100, 0),
    (16, true, 100, 0),
    (17, true, 100, 0),
    (18, true, 100, 0),
    (19, true, 100, 0),
    (20, true, 100, 0),
    (21, true, 100, 0),
    (22, true, 100, 0),
    (23, true, 100, 0),
    (24, true, 100, 0),
    (25, true, 100, 0),
    (26, true, 100, 0),
    (27, true, 100, 0),
    (28, true, 100, 0),
    (29, true, 100, 0),
    (30, true, 100, 0),
    (31, true, 100, 0),
    (32, true, 100, 0),
    (33, true, 100, 0),
    (34, true, 100, 0),
    (35, true, 100, 0
);

/* depots table values */
INSERT INTO depots VALUES(
    0, "Leeds City Bus Station Depot", "New York St, Leeds LS2 7HU", true, "/static/BusStation.jpg"),
    (1, "Owl Cotes Shopping Centre Depot", "Owlcotes shopping centre, Stanningley, Leeds, Pudsey LS28 6AR", true, "/static/OwlCotes.jpg"),
    (2, "Leeds Trinity University Depot", "Brownberrie Ln, Horsforth, Leeds LS18 5HD", true, "/static/LeedsTrinity.jpg"
);

/* users table values */
INSERT INTO users VALUES(
    0, "N/A", "N/A", "ADMIN", "8021629540015279313889895012696393549545", "N/A", "N/A"
);

/* scooter_depot link table values */
INSERT INTO scooter_depot VALUES(
    0, 0),
    (1, 0),
    (2, 0),
    (3, 0),
    (4, 0),
    (5, 0),
    (6, 0),
    (7, 0),
    (8, 0),
    (9, 0),
    (10, 0),
    (11, 0),
    (12, 1),
    (13, 1),
    (14, 1),
    (15, 1),
    (16, 1),
    (17, 1),
    (18, 1),
    (19, 1),
    (20, 1),
    (21, 1),
    (22, 1),
    (23, 1),
    (24, 2),
    (25, 2),
    (26, 2),
    (27, 2),
    (28, 2),
    (29, 2),
    (30, 2),
    (31, 2),
    (32, 2),
    (33, 2),
    (34, 2),
    (35, 2
);

/* Note: These are for testing only. Enabling them will cause bugs as they do not change the availability of the selected scooters. */
/* user_depots link table values */
/*INSERT INTO user_depots VALUES(
    0, 0
);*/

/* user_scooters link table values */
/*INSERT INTO user_scooters VALUES(
    0, 0
);*/