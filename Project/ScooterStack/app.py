#(This file controls the website)
from flask import Flask, render_template, url_for, request, redirect
from flask_login import LoginManager, UserMixin, current_user, login_user, logout_user
import sys
from util import get_depots, validate_user, get_user, get_user_info, create_user_account, delete_user_account, depot_search, scooter_book, scooter_unbook, random_scooter_info, charge_scooters, set_depot_status, user_purge
from user import User

#User accounts
login_manager = LoginManager()
app = Flask(__name__)
app.secret_key = "OHBOY"
login_manager.init_app(app)
@login_manager.user_loader
def load_fuser(id):
    user_check = get_user(id)
    if len(user_check) <= 0:
        return User(-999, "-999USERDOESNOTEXIST")
    else:
        return User(user_check[0], user_check[1])
def get_username_auth():
    if current_user.is_authenticated:
        user=current_user.username
    else:
        user=None
    return user

#Main Routes
@app.route('/') #Home Page
def home():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('home.html', page_name="Home", user=get_username_auth())
@app.route('/about') #About Page
def about():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('about.html', page_name="About", user=get_username_auth())
@app.route('/depots') #Depots Page
def depots():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    return render_template('depots.html', page_name="Depots", depot_info=get_depots(False, None), user=get_username_auth())
@app.route('/booking/failed=<booking_failed>')
@app.route('/booking/failed=<booking_failed>/search=<search>')
@app.route('/booking/falied=<booking_falied>')
def booking(booking_failed, search=None): #Database search page
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            if search != None:
                search = search.upper()
                if search[0] != "L" or search[1] != "S":
                    return redirect('/booking/failed=True')
                elif len(search) >= 3:
                    if str(search[2]).isdigit is False:
                        return redirect('/booking/failed=True')
                elif len(search) < 3:
                    return redirect('/booking/failed=True')
            return render_template('booking.html', page_name="Booking", user=get_username_auth(), depots=depot_search(search), search=search)
    else:
        return redirect('/login/failed=False')
@app.route('/booking/depots/<depot_name>') #Scooter booking page
def booking_scooters(depot_name):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            return render_template('booking_scooters.html', page_name="Booking", user=get_username_auth(), depot_info=get_depots(True, depot_name))
    else:
        return redirect('/login/failed=False')
@app.route('/booking/depots/<depot_name>/userbook') #Page that books scooters for the user
def booking_scooter_set(depot_name):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            scooter_book(depot_name, current_user.username)
            return redirect('/booking/depots/success')
    else:
        return redirect('/login/failed=False')
@app.route('/booking/depots/success')
def booking_success(): #Page that informs the user of successfull booking
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            return render_template('booking_success.html', page_name="Success", user=get_username_auth())
    else:
        return redirect('/login/failed=False')
@app.route('/booking/depots/unbooking')
def unbooking():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            scooter_unbook(current_user.username)
            return redirect('/booking/depots/unbooked')
    else:
        return redirect('/login/failed=False')
@app.route('/booking/depots/unbooked')
def unbooked():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
        else:
            return render_template('booking_unbooked.html', page_name="Unbooked", user=get_username_auth(), info=random_scooter_info())
    else:
        return redirect('/login/failed=False')

#Account Routes
@app.route('/login/failed=<login_failed>') #Login Page
def login(login_failed):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    print(login_failed, file=sys.stderr)
    if current_user.is_authenticated:
        return redirect('/')
    else:
        return render_template('login.html', page_name="Login", login_status=login_failed)
@app.route('/login/userinfo=<user_info>') #Login Check Page. Note: User is immediately redirected and no page should be returned.
def login_auth(user_info):
    rem_ip = str(request.remote_addr)
    if user_info == "ADMIN+ADMIN":
        if rem_ip != "10.110.101.123" and rem_ip != "192.168.1.84" and rem_ip != "192.168.1.21":
            return redirect('/login/failed=False')
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        return redirect('/')
    else:
        user = validate_user(user_info)
        try:
            login_user(user)
            return redirect('/')
        except:
            return redirect('/login/failed=True')
@app.route('/user/<username>') #User's account page
def user_account_page(username):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        if current_user.username == username:
            return render_template('account.html', page_name=username, user=get_username_auth(), userinfo=get_user_info(username))
        else:
            return redirect('/login/failed=False')
    else:
        return redirect('/login/failed=False')
@app.route('/logout')
def logout():
    print("Remote IP: " + str(request.remote_addr))
    logout_user()
    return redirect('/')
@app.route('/signup/failed=<signup_failed>') #Signup page
def signup(signup_failed):
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    if current_user.is_authenticated:
        return redirect('/')
    else:
        return render_template('signup.html', page_name="Signup", user=get_username_auth())
@app.route('/signup/create/userinfo=<user_info>')
def create_account(user_info): #Account Creation
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        if current_user.username == "-999USERDOESNOTEXIST":
            return redirect('/logout')
    signup_status = create_user_account(user_info)
    if signup_status is True:
        return redirect('/login/failed=False')
    else:
        return redirect('/signup/failed=True')
@app.route('/user/delete')
def delete_account():
    print("Remote IP: " + str(request.remote_addr))
    if current_user.is_authenticated:
        delete_user_account(current_user.username)
        return redirect('/logout')
    else:
        return redirect('/login/failed=False')

#This last route is the admin page. It will be used to control the website without having to use the CLI.
@app.route('/admin/command=<command>')
def admin(command):
    print("Remote IP: " + str(request.remote_addr))
    rem_ip = str(request.remote_addr)
    if rem_ip != "10.110.101.123" and rem_ip != "192.168.1.84" and rem_ip != "192.168.1.21":
        return redirect('/login/failed=False')
    else:
        if current_user.is_authenticated:
            if current_user.username == "-999USERDOESNOTEXIST":
                return redirect('/logout')
            elif current_user.username != "ADMIN":
                return redirect('/logout')
            else:
                if command != None and command != "nocommand":
                    if command == "charge":
                        charge_scooters()
                    if command == "open_depots":
                        set_depot_status(True)
                    if command == "close_depots":
                        set_depot_status(False)
                    if command == "user_purge":
                        user_purge()
                return render_template('admin_page.html', page_name="Admin Control", user=get_username_auth())
        else:
            return redirect('/login/failed=False')
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=False)