//This script is very similar to login.js but it creates a URL to signup rather than to login.
console.log("Signup.js Loaded!");
let signupForenameBox = document.getElementById("signup_forename");
let signupSurnameBox = document.getElementById("signup_surname");
let signupUsernameBox = document.getElementById("signup_username");
let signupPasswordBox = document.getElementById("signup_password");
let signupEmailBox = document.getElementById("signup_email");
let signupPhoneBox = document.getElementById("signup_phone_no");
let signupSubmitButton = document.getElementById("signup_subbutton");
let signupError = document.getElementById("signup_div_error_msg");
if (signupForenameBox == null || signupSurnameBox  == null || signupUsernameBox == null || signupPasswordBox == null || signupEmailBox == null || signupPhoneBox == null || signupSubmitButton == null || signupError == null){
    console.log("Signup.js: Signup screen not found. Script not running.");
}
else{
    if (window.location.href.includes("failed%3DFalse")){//Display login error if failed=True is in the URL or hide the error text if it is False.
        signupError.style.visibility = 'hidden';
    }
    else if (window.location.href.includes("failed=False")){
        loginError.style.visibility = "hidden";
    }
    else{
        signupError.innerHTML = "Singup failed, please try again.";
    }
    const signupButton = function(){
        console.log("Sign");
        if (signupForenameBox.value === "" || signupSurnameBox.value === "" || signupUsernameBox.value === "" || signupPasswordBox.value === ""){
            //This code changes the error message to reflect that the text boxes are empty and also makes the error message visible if the script previously made it hidden.
            signupError.innerHTML = "Please fill in all entries marked with an asterisk. (*)";
            signupError.style.visibility = "visible";
        }
        else if (signupUsernameBox.value.toUpperCase() === "-999USERDOESNOTEXIST" || signupUsernameBox.value.toUpperCase() === "ADMIN"){ //Check for debug usernames
            signupError.innerHTML = "Invalid Username";
            signupError.style.visibility = "visible";
        }
        else if (signupPasswordBox.value.includes("1234")){
            signupError.innerHTML = "Please enter a stronger password";
            signupError.style.visibility = "visible";
        }
        else if (signupPasswordBox.value.includes(signupForenameBox.value) || signupPasswordBox.value.includes(signupSurnameBox.value) || signupPasswordBox.value.includes(signupUsernameBox.value)){ //Make sure the user does not enter already entered info as their password
            signupError.innerHTML = "Please ensure your password does not contain your username or forename/surname.";
            signupError.style.visibility = "visible";
        }
        else{
            let signupURL = "/signup/create/userinfo="; //This part creates the URL which is "/signup/create/userinfo=" and then the text from the text boxes separated by pluses.
            signupURL = signupURL + signupForenameBox.value;
            signupURL = signupURL + "+" + signupSurnameBox.value;
            signupURL = signupURL + "+" + signupUsernameBox.value;
            signupURL = signupURL + "+" + signupPasswordBox.value;
            signupURL = signupURL + "+" + signupEmailBox.value;
            signupURL = signupURL + "+" + signupPhoneBox.value;
            window.location.replace(signupURL); //This redirects to the created URL at which point the main app.py will take control of creating the account for the user.
        }
    }
    console.log("Signup.js: Waiting for user to click submit button...")
    signupSubmitButton.addEventListener("click", signupButton);//Adds an event listener to the login button. When the user clicks it, it then runs the above function.
    //These next two event listeners wait for the user to press enter while writing in a text box and, if they do, it will simulate them clicking the signup button so they don't need to.
    signupForenameBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
    signupSurnameBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
    signupUsernameBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
    signupPasswordBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
    signupEmailBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
    signupPhoneBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
            event.preventDefault();
            signupSubmitButton.click();
        }
    });
}