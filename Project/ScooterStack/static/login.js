//This script gets the data from the text boxes on the login page and turns that data into a URL to log the user in. It also controls the login error messages.
console.log("Login.js Loaded!");
let loginUsernameBox = document.getElementById("login_username");
let loginPasswordBox = document.getElementById("login_password");
let loginSubmitButton = document.getElementById("login_subbutton");
let loginError = document.getElementById("login_div_error_msg");
if (loginUsernameBox == null || loginPasswordBox == null || loginSubmitButton == null || loginError == null){
    console.log("Login.js: Login screen not found. Script not running.");
}
else{
    if (window.location.href.includes("failed%3DFalse")){//Display login error if failed=True is in the URL or hide the error text if it is False.
        loginError.style.visibility = "hidden";
    }
    else if (window.location.href.includes("failed=False")){
        loginError.style.visibility = "hidden";
    }
    else{
        loginError.innerHTML = "Login failed, please try again.";
    }
    const loginButton = function(){//This function checks to see if the text boxes are empty and, if they aren't, it then creates a URL from the entered text and redirects to it.
        if (loginUsernameBox.value === "" || loginPasswordBox.value === ""){
            //This code changes the error message to reflect that the text boxes are empty and also makes the error message visible if the script previously made it hidden.
            loginError.innerHTML = "Please fill in both boxes.";
            loginError.style.visibility = "visible";
        }
        else if (loginUsernameBox.value.toUpperCase() === "-999USERDOESNOTEXIST" || loginUsernameBox.value.toUpperCase() === "ADMIN"){ //Check for debug usernames
            loginError.innerHTML = "Invalid Username";
            loginError.style.visibility = "visible";
        }
        else{
            let loginURL = "/login/userinfo="; //This part creates the URL which is "login/userinfo=" and then the text from the text boxes separated by pluses.
            loginURL = loginURL + loginUsernameBox.value;
            loginURL = loginURL + "+" + loginPasswordBox.value;
            window.location.replace(loginURL); //This redirects to the created URL at which point the main app.py will take control of logging the user in.
        }
    }
    console.log("Login.js: Waiting for user to click submit button...");
    loginSubmitButton.addEventListener("click", loginButton); //Adds an event listener to the login button. When the user clicks it, it then runs the above function.
    //These next two event listeners wait for the user to press enter while writing in a text box and, if they do, it will simulate them clicking the login button so they don't need to.
    loginUsernameBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
         event.preventDefault();
         loginSubmitButton.click();
        }
    });
    loginPasswordBox.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
         event.preventDefault();
         loginSubmitButton.click();
        }
    });
}