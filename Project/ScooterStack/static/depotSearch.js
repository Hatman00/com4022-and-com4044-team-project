console.log("depotSearch.js Loaded!");
depotSearchBox = document.getElementById("booking_postcode");
depotSearchButton = document.getElementById("booking_searchbutton");
errorMsg = document.getElementById("booking_div_error_msg");
console.log(errorMsg);
if (window.location.href.includes("failed%3DFalse")){//Display login error if failed=True is in the URL or hide the error text if it is False.
    errorMsg.style.visibility = "hidden";
}
else if (window.location.href.includes("failed=False")){
    errorMsg.style.visibility = "hidden";
}
else{
    errorMsg.innerHTML = "Search failed, please try again.";
}
const searchButton = function(){
    searchURL = "/booking/failed=False/search=";
    searchURL = searchURL + depotSearchBox.value;
    window.location.replace(searchURL);
}
console.log("depotSearch.js: Waiting for user to click button...");
depotSearchButton.addEventListener("click", searchButton);
depotSearchBox.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) { //13 is the keycode for the ENTER key.
     event.preventDefault();
     depotSearchButton.click();
    }
});