console.log("Waiting for 5 seconds before redirect...")
function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}
delay(5000).then(() => window.location.replace('/'));