import mysql
import mysql.connector
import sys
from config import db_config
from user import User
import random
#This file should contain the functions that communicate with the database.
#The following functions do not access the database.
def basic_hash(text):
    text_int_list = []
    for item in text:
        text_int_list.append(str(ord(item)))
    text_int_list = ''.join(text_int_list)
    int_text = int(text_int_list)
    mult = 6571124425203826399305484198005679260949
    remainder = 8334203544772658688568069417389950195363
    #hashed_text = int_text * len(str(int_text))
    hashed_text = int_text * mult % remainder #Thanks to Peter from the Leeds Discord server for this better hash.
    return hashed_text
def random_scooter_info():
    #I couldn't really find anything on the health benefits of electric scooters so I took what I found for push/kick scooters and halved it twice.
    #So the original number I found was 204 calories per 1 mile and the result of the above division is 51 calories.
    #So for every mile, add 51 calories.
    scooter_miles = random.randint(2, 30)
    scooter_cal = 51 * scooter_miles
    scooter_co2 = 411 * scooter_miles
    #if scooter_co2 > 1100:
    #    scooter_co2 /= 1000
    #    scooter_co2 = round(scooter_co2, 2)
    scooter_usage_info = {
        "scooter_mileage": scooter_miles,
        "scooter_cal": scooter_cal,
        "scooter_av_speed": random.randint(5, 15),
        "scooter_co2_prevented": scooter_co2
    }
    return scooter_usage_info

#These function gets all of the depots and their info from the database
def get_depots(is_name, name):
    if is_name is False:
        depot_list = []
        database = mysql.connector.connect(**db_config)
        cursor = database.cursor()
        cursor.execute("SELECT * FROM depots")
        all_depots = cursor.fetchall()
        for depots in all_depots:
            depot_list.append({'depot_id': depots[0], 'depot_name': depots[1], 'depot_loc': depots[2], 'depot_status': depots[3], "depot_imagename": depots[4]})
        return depot_list
    else:
        depot_list = {}
        database = mysql.connector.connect(**db_config)
        cursor = database.cursor()
        cursor.execute("SELECT * FROM depots WHERE depot_name = %s", (str(name),))
        for item in cursor.fetchall():
            depot_list["depot_id"] = item[0]
            depot_list["depot_name"] = item[1]
            depot_list["depot_location"] = item[2]
            depot_list["depot_open"] = item[3]
            depot_list["depot_imagename"] = item[4]
        scooter_count = 0
        cursor.execute("SELECT * FROM scooter_depot WHERE depot_id = %s", (str(depot_list["depot_id"]),))
        scooter_depot_list = cursor.fetchall()
        for item in scooter_depot_list:
            cursor.execute("SELECT * FROM scooters WHERE scooter_id = %s", (str(item[0]),))
            for scooter in cursor.fetchall():
                scooter_status = scooter[1]
            if str(scooter_status) == "1":
                scooter_count += 1
        depot_list["depot_scooter_count"] = scooter_count
        return depot_list

#Attempts to sign the user in
def validate_user(uinfo):
    uinfo = uinfo.split("+")
    current_user = None
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(uinfo[0]),))
    retrieved_user = cursor.fetchall()
    if len(retrieved_user) > 0:
        print("Username Accepted", file=sys.stderr)
        for item in retrieved_user:
            uinfo[1] = basic_hash(uinfo[1])
            if str(uinfo[1]) == str(item[4]):
                print("Password Accepted", file=sys.stderr)
                current_user = User(item[0], item[3])
            else:
                print(uinfo[1] == item[4])
                print("Password Denied", file=sys.stderr)
    else:
        print("Username Denied")
    return current_user

def get_user(id): #Retrieves the user information for a user that has already signed in
    user = []
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_id_no = '" + str(id) + "'")
    cfetch = cursor.fetchall()
    for item in cfetch:
        user.append(item[0])
        user.append(item[3])
        user.append(item[5])
    return user

def get_user_info(username): #Gets the user's information from the database
    user_info = {}
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(username),))
    retrieved_user = cursor.fetchall()
    if len(retrieved_user) > 0:
        for item in retrieved_user:
            user_info["user_id"] = item[0]
            user_info["user_forename"] = item[1]
            user_info["user_surname"] = item[2]
            user_info["user_username"] = item[3]
            user_info["user_email"] = item[5]
            user_info["user_phone"] = item[6]
    cursor.execute("SELECT * FROM user_scooters WHERE user_id_no = %s", (str(user_info["user_id"]),))
    scooters = cursor.fetchall()
    for item in scooters:
        cursor.execute("SELECT * FROM scooters WHERE scooter_id = %s", (str(item[0]),))
        scooter_info = cursor.fetchall()
        user_info["scooter_id"] = scooter_info[0][0]
        user_info["scooter_charge"] = scooter_info[0][2]
        user_info["scooter_times_used"] = scooter_info[0][3]
    cursor.execute("SELECT * FROM user_depots WHERE user_id_no = %s", (str(user_info["user_id"]),))
    depots = cursor.fetchall()
    for item in depots:
        cursor.execute("SELECT depot_name FROM depots WHERE depot_id = %s", (str(item[1]),))
        depot_name = cursor.fetchall()
        user_info["depot_name"] = depot_name[0][0]
    if "depot_name" not in user_info:
        user_info["depot_name"] = None
    return user_info

def create_user_account(userinfo): #Creates the user account
    success_status = False
    userinfo = userinfo.split("+")
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(userinfo[2]),))
    retrieved_user = cursor.fetchall()
    print("Users found: " + str(len(retrieved_user)), file=sys.stderr)
    if len(retrieved_user) <= 0:
        #0, "TEST", "USER", "TESTUSER", "ADMIN", "N/A", "N/A"
        cursor.execute("SELECT user_id_no FROM users")
        new_user_id = len(cursor.fetchall())
        if (userinfo[4] == ""):
            userinfo[4] = "N/A"
        if (userinfo[5] == ""):
            userinfo[5] = "N/A"
        #Password needs to be HASHED!!!! Get on that.
        userinfo[3] = basic_hash(userinfo[3])
        cursor.execute("INSERT INTO users VALUES(%s, %s, %s, %s, %s, %s, %s)", (new_user_id, userinfo[0], userinfo[1], userinfo[2], userinfo[3], userinfo[4], userinfo[5]))
        database.commit()
        success_status = True
    return success_status

def delete_user_account(uname): #Deletes the user's account after deleting their id from any linked tables.
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT user_id_no FROM users WHERE user_uname = %s", (str(uname),))
    for item in cursor.fetchall():
        user_id = item
    cursor.execute("DELETE FROM user_depots WHERE user_id_no = %s", (str(user_id[0]),))
    database.commit()
    cursor.execute("SELECT * FROM user_scooters WHERE user_id_no = %s", (str(user_id[0]),))
    scooters = cursor.fetchall()
    for item in scooters:
        cursor.execute("UPDATE scooters SET available=true WHERE scooter_id=%s", (str(item[0]),))
        database.commit()
    cursor.execute("DELETE FROM user_scooters WHERE user_id_no = %s", (str(user_id[0]),))
    database.commit()
    cursor.execute("DELETE FROM users WHERE user_uname = %s", (str(uname),))
    database.commit()
    success_status = True

def depot_search(address): #Searches for the nearest depot based on the entered post code.
    if (address != None):
        try:
            print(len(address), file=sys.stderr)
            if len(address) >= 4:
                address = address[2] + address[3]
            else:
                address = address + " "
                address = address[2] + address[3]
            address = int(address)
            depot_info = {}
            if address <= 2 and address <= 10:
                depot_id = 0
            elif address > 10 and address <= 23:
                depot_id = 2
            else:
                depot_id = 1
            database = mysql.connector.connect(**db_config)
            cursor = database.cursor()
            cursor.execute("SELECT * FROM depots WHERE depot_id = %s", (str(depot_id),))
            for item in cursor.fetchall():
                depot_info["depot_id"] = item[0]
                depot_info["depot_name"] = item[1]
                depot_info["depot_location"] = item[2]
                depot_info["depot_open"] = item[3]
                depot_info["depot_imagename"] = item[4]
            return depot_info
        except:
            return None
    else:
        return None

def scooter_book(depot_name, username): #Sets the first available scooter in the selected depot to be booked by the user using the user_scooters link table (and user_depots link table) and sets the scooter's availability to be false.
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(username),))
    user_id = cursor.fetchall()
    user_id = user_id[0][0]
    #cursor.execute("SELECT * FROM user_scooters WHERE user_id_no = %s", (str(user_id))) <- Can't remeber why I wrote this? Leaving it here for now.
    chosen_scooter = None
    cursor.execute("SELECT * FROM scooter_depot WHERE depot_id = %s", (str(depot_name),))
    scooter_list = cursor.fetchall()
    for item in scooter_list:
        print(item)
        cursor.execute("SELECT * FROM scooters WHERE scooter_id = %s", (str(item[0]),))
        scooter = cursor.fetchall()
        print(scooter)
        if str(scooter[0][1]) == "1": #Get the first scooter from the selected depot that is available, then break the loop. (Sorry if this doesn't make sense)
            chosen_scooter = scooter[0][0]
            break
    cursor.execute("SELECT * FROM user_depots WHERE user_id_no = %s", (str(user_id),))
    if len(cursor.fetchall()) >= 1: #If the user has already booked a scooter, it updates the link user_depots table to the new depot. Else, it inserts their ID and the depot ID into a new column of the link table.
        cursor.execute("UPDATE user_depots SET depot_id=%s WHERE user_id_no=%s", (str(depot_name), str(user_id),))
    else:
        cursor.execute("INSERT INTO user_depots VALUES(%s, %s)", (str(user_id), str(depot_name),))
    database.commit()
    cursor.execute("SELECT * FROM user_scooters WHERE user_id_no = %s", (str(user_id),))
    old_scooter = cursor.fetchall()
    if len(old_scooter) >= 1: #Ditto to the above depot code except for the scooters.
        cursor.execute("UPDATE user_scooters SET scooter_id=%s WHERE user_id_no=%s", (str(chosen_scooter), str(user_id),))
        database.commit()
        cursor.execute("UPDATE scooters SET available=true WHERE scooter_id = %s", (str(old_scooter[0][0]),))
    else:
        cursor.execute("INSERT INTO user_scooters VALUES(%s, %s)", (str(chosen_scooter), str(user_id),))
    database.commit()
    cursor.execute("UPDATE scooters SET available=false WHERE scooter_id = %s", (str(chosen_scooter),)) #Set the selected scooter to no longer be available.
    database.commit()

def scooter_unbook(username): #Removes the scooter from the user that booked it.
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("SELECT * FROM users WHERE user_uname = %s", (str(username),))
    user_id = cursor.fetchall()
    user_id = user_id[0][0]
    cursor.execute("SELECT * FROM user_scooters WHERE user_id_no = %s", (str(user_id),))
    scooter_id = cursor.fetchall()
    if len(scooter_id) >= 1:
        scooter_id = scooter_id[0][0]
        cursor.execute("SELECT * FROM scooters WHERE scooter_id = %s", (str(scooter_id),))
        old_scooter = cursor.fetchall()
        if len(old_scooter) >= 1: #Decrese the scooter charge by a random amount between 1 and 10 and also increase the times used by 1. (And make the scooter available)
            old_charge = int(old_scooter[0][2])
            old_times_used = int(old_scooter[0][3])
            cursor.execute("UPDATE scooters SET available=true WHERE scooter_id=%s", (str(scooter_id),))
            database.commit()
            old_charge = old_charge - random.randint(1, 10)
            old_times_used += 1
            cursor.execute("UPDATE scooters SET charge=%s WHERE scooter_id=%s", (str(old_charge), str(scooter_id),))
            database.commit()
            cursor.execute("UPDATE scooters SET times_used=%s WHERE scooter_id=%s", (str(old_times_used), str(scooter_id),))
            database.commit()
            if old_charge <= 10:
                cursor.execute("UPDATE scooters SET available=false WHERE scooter_id=%s", (str(scooter_id),))
                database.commit()
        cursor.execute("DELETE FROM user_scooters WHERE scooter_id = %s ", (str(scooter_id),))
        database.commit()
    cursor.execute("SELECT * FROM user_depots WHERE user_id_no = %s", (str(user_id),))
    depot_id = cursor.fetchall()
    if len(depot_id) >= 1:
        depot_id = depot_id[0][1]
        cursor.execute("DELETE FROM user_depots WHERE depot_id = %s", (str(depot_id),))
        database.commit()

#The following functions are for admin use only and are ONLY to be called on the ADMIN page.
def charge_scooters():
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("UPDATE scooters SET charge=100")
    database.commit()
def set_depot_status(status):
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    if status is True:
        cursor.execute("UPDATE depots SET depot_open=true")
    else:
        cursor.execute("UPDATE depots SET depot_open=false")
    database.commit()
def user_purge():
    database = mysql.connector.connect(**db_config)
    cursor = database.cursor()
    cursor.execute("DELETE FROM user_scooters")
    database.commit()
    cursor.execute("DELETE FROM user_depots")
    database.commit()
    cursor.execute("DELETE FROM users WHERE user_uname != 'ADMIN'")
    database.commit()
    cursor.execute("UPDATE scooters SET available=true")
    database.commit()